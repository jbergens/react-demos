import { observer, inject } from 'mobx-react';
import CounterView from './CounterView';

export default CounterView;

// Wrap the CounterView and connect it to the observer implementation in mobx
// const ConnectedCounterView = inject("CounterStore", observer(CounterView));
/*
const ConnectedCounterView = inject(observer(CounterView), "CounterStore");

export default ConnectedCounterView;
*/
