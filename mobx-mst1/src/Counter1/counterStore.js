import { types } from 'mobx-state-tree';

// setup logger and inject it when the store is created
const logger = {
    log(msg) {
        console.log(msg)
    }
};

const CounterStore = types.model('Counters', {
    counters: types.map(types.number),
})
.actions(self => ({
    increase(key) {
        console.log("increase", key);
        // Needs ...counters.set/get(key) when counters is an observable map!
        let oldValue = self.counters.get(key) || 0;
        console.log("increase old value", oldValue);
        self.counters.set(key, oldValue + 1);
    },
    decrease(key) {
        console.log("decrease", key);
        let oldValue = self.counters.get(key) || 0;
        console.log("decrease old value", oldValue);
        //self.counters[key] = oldValue - 1;
        self.counters.set(key, oldValue - 1);
    }
}))
.create({
    counters: {a: 0, b: 0},
})
// , {
//     logger: logger
// })

export default CounterStore;
