import React, { Component } from 'react';
import { Provider } from "mobx-react";
import CounterStore from './counterStore';
import MainView from './MainView';

//const counterStore = CounterModel.create();

export default function App1() {
    return (
        <Provider counterStore={CounterStore}>
            <MainView />
        </Provider>
    );
}
