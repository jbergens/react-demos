import React, { Component } from 'react';
// import { NICE, SUPER_NICE, GOOD } from './colors';
import { NICE } from '../colors';
import ConnectedCounterView from './ConnectedCounterView';
// import PureCounter from './PureCounter';
import CounterToolbar from './CounterToolbar';

export default class MainView extends Component {
    render() {
        return (
            <div>
                <h1>Mobx Demo 1</h1>
                <p>Add support for having a third counter.
                </p>
                <div>
                    <ConnectedCounterView name="Alfa" />
                    The counter and the button don&apos;t have to be next to each other here.
                    <ConnectedCounterView name="Beta" counter="a" color={NICE} />
                </div>

                <p>Here the buttons are separate.</p>
                <CounterToolbar counter="a" />
            </div>
        );
    }
}
