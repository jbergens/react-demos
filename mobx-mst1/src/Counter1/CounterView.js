import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';
import PropTypes from 'prop-types';
import { /* NICE, SUPER_NICE, */ GOOD } from '../colors';
// Using the store directly
// import counterStore from './counterStore';

@inject("counterStore")
@observer
class Counter extends Component {
    render() {
        const key = this.props.counter || 'a';
        // console.log(`Counter[${key}].render()`, counterStore.counters);
        console.log(`Counter[${key}].render()`, this.props.counterStore.counters);

        //const myValue = counterStore.counters[key] || 0; // Use the model/store
        //const myValue = this.props.counterStore.counters[key] || 0; // Use the model/store
        // Needs ...counters.get(key) when counters is an observable map!
        const myValue = this.props.counterStore.counters.get(key) || 0; // Use the model/store

        return (
            <div>
                <h1 style={{ color: this.props.color }}>
                    Counter ({this.props.name}:{key}): {myValue}
                </h1>
            </div>
        );
    }
}

Counter.propTypes = {
    name: PropTypes.string.isRequired,
    counter: PropTypes.string,
    color: PropTypes.string,
};

Counter.defaultProps = {
    counter: 'a',
    color: GOOD,
};

export default Counter;
