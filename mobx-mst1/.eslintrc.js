module.exports = {
    "extends": "airbnb",
    globals: {
        "document": false,
        "it": false
    },
    rules: {
        "indent": ["warn", 4],
        "no-console": 0,
        "react/jsx-filename-extension": [1, { "extensions": [".js", ".jsx"] }],
        "react/jsx-indent": [1, 4],
        "react/jsx-indent-props": [1, 4],
        "react/jsx-one-expression-per-line": [0, {"allow": "literal"}],
        "react/prefer-stateless-function" : 0,
        "react/destructuring-assignment" : 0,
        "linebreak-style": 0
    }
};
