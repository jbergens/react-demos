#Simple Mobx State Tree demo v1

This demo contains some simple react code with the
Mobx State Tree state management library. It uses the create-react-app to
configure some tools like WebPack.

It can be started by running the following commands:

```
> yarn install
> yarn start
```
