import React, { Component } from 'react';
import StdComponent from './StdComponent';
import FuncComponentA from './FuncComponentA';
import FuncComponentB from './FuncComponentB';

class SimpleMainView extends Component {
    render() {
        return (
            <div>
                <p>React is running!</p>
                <StdComponent name="Donald Duck" />
                <FuncComponentA name="Daisy" />
                <FuncComponentB name="Daisy" />
            </div>
        );
    }
}

export default SimpleMainView;
