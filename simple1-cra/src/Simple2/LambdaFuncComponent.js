import React from 'react';
import PropTypes from 'prop-types';
import myStyles from './helloBoxStyle';

const LambdaFuncComponent = ({ name }) => (
    <div style={myStyles}>
        <h3>Hi {name}!</h3>
        <p>This is a stateless component from a lambda.</p>
    </div>
);

LambdaFuncComponent.propTypes = {
    name: PropTypes.string.isRequired,
};

export default LambdaFuncComponent;
