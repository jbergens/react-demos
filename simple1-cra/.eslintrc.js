module.exports = {
    "extends": "airbnb",
    globals: {
        "document": false,
        "it": false
    },
    rules: {
        "indent": ["warn", 4],
        "react/jsx-filename-extension": [1, { "extensions": [".js", ".jsx"] }],
        "react/jsx-indent": [1, 4],
        "react/jsx-indent-props": [1, 4],
        "react/jsx-one-expression-per-line": [0, {"allow": "literal"}],
        "react/prefer-stateless-function" : 0,
        "react/destructuring-assignment" : 0,
        "linebreak-style": 0,
        "func-names": 0,
    }
};

// Add the row below to the settings if decorators or @-signs are needed. This also requires that
// the package babel-eslint is installed with yarn/npm.
// parser: "babel-eslint",

// Add `"no-console": 0, ` if console logging should be allowed
