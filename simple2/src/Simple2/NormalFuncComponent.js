import React from 'react';
import PropTypes from 'prop-types';
import myStyles from './helloBoxStyle';

function NormalFuncComponent({ name }) {
    return (
        <div style={myStyles}>
            <h3>Hi {name}!</h3>
            <p>This is a stateless component with style (NormalFuncComponent).</p>
        </div>
    );
}

NormalFuncComponent.propTypes = {
    name: PropTypes.string.isRequired,
};

export default NormalFuncComponent;
