# Simple React 1

This is a demo project showing some very simple React components. Use a starting point for experimenting with React.
The setup contains some eslint settings.

The project is created with Vite and contains a local development server. See below for details.

*NOTE! Vite requires usage of the jsx extension on files that contains jsx*

It can be started by running the following commands:

```
> npm install
> npm dev
```

# About Vite

The project was crated using this Vite template:

[https://github.com/jamosaur/vite-react-tailwind-rtk](https://github.com/jamosaur/vite-react-tailwind-rtk)

Read about Vite on [https://vitejs.dev/](https://vitejs.dev/)

## Known problems

Tailwind css does not seem to update automatically (or at all?)
