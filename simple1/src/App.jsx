import React from 'react';
// eslint-disable-next-line
import SimpleMainView from './Simple/SimpleMainView.jsx';
// eslint-disable-next-line
import Simple2MainView from './Simple2/Simple2MainView.jsx';

const App = () => {
  return (
    <div className="p-6 bg-indigo-200">
      <div className="">
          <h1>Example with React components</h1>
          <p className="bg-orange-300">Try to change this text and watch your web browser.
          Change to one of the extra components that are not used yet.
          </p>
          <SimpleMainView />
      </div>
    </div>
  );
};

export default App;
