import React from 'react';

// Using normal function to create a component

function FuncComponentA(props) {
    return (
        <div className="App-box">
            <p>Hi {props.name}! </p>
            <p>This is a functional component with JSX.</p>
        </div>
    );
}

export default FuncComponentA;
