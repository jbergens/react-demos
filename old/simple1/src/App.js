import React, { Component } from 'react';
// eslint-disable-next-line
import SimpleMainView from './Simple/SimpleMainView';
// eslint-disable-next-line
import Simple2MainView from './Simple2/Simple2MainView';

export default class App extends Component {
    render() {
        // const activeComponent = <SimpleMainView />;
        const activeComponent = <Simple2MainView />;

        return (
            <div>
                <h1>Example with React components</h1>
                <p>Try to change this text and watch your web browser.
                Change to one of the extra components that are not used yet.
                </p>
                {activeComponent}
            </div>
        );
    }
}
