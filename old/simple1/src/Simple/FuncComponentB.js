import React from 'react';

const myStyles = {
    border: 'solid 1px black',
    paddingLeft: 10,
    paddingRight: 10,
    backgroundColor: 'lightskyblue',
};

// Using arrow function to create a component

const FuncComponentB = ({ name }) => (
    <div style={myStyles}>
        <p>Hi {name}!</p>
        <p>This is a stateless component with style.</p>
    </div>
);

export default FuncComponentB;
