# Simple React 2

This is a demo project showing some very simple react components. Use a starting point for experimenting with react. 
The setup contains the test library tape and some eslint settings.

The project is created with create-react-app version 2 (CRA) and
contains a local development server. See below for details.

It can be started by running the following commands:

```
> yarn install
> yarn start
```

# About CRA

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

Read about CRA [here](cra.md)
