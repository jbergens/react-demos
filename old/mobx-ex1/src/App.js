import React from 'react';
// eslint-disable-next-line
import Intro from './Intro';
// eslint-disable-next-line
import C1MainView from './Counter1/MainView';
// eslint-disable-next-line
import C2MainView from './Counter2/MainView';

export default function App() {
    // return <Intro />;
    // return <C1MainView />;
    return <C2MainView />;
}
