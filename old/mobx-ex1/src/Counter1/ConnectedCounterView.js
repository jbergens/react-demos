import { observer } from 'mobx-react';
import CounterView from './CounterView';

// Wrap the CounterView and connect it to the observer implementation in mobx
const ConnectedCounterView = observer(CounterView);

export default ConnectedCounterView;
