import React, { Component } from 'react';
// import { NICE, SUPER_NICE, GOOD } from './colors';
import { NICE } from '../colors';
import ConnectedCounterView from './ConnectedCounterView';
import PureCounter from './PureCounter';
import CounterToolbar from './CounterToolbar';

export default class MainView extends Component {
    render() {
        return (
            <div>
                <h1>Mobx Demo 2</h1>
                <p>Add support for having a third counter.
                </p>
                <ConnectedCounterView name="Alfa" />
                <div>
                    The counter and the button don&apos;t have to be next to each other here.
                    <ConnectedCounterView name="Beta" counter="b" color={NICE} />
                    <PureCounter name="Gamma" counter="a" />
                </div>

                <p>Here the buttons are separate.</p>
                <CounterToolbar counter="a" />
                <CounterToolbar counter="b" />
            </div>
        );
    }
}
