import React, { Component } from 'react';
// import { NICE, SUPER_NICE, GOOD } from './colors';
import { NICE } from './colors';
// import ConnectedCounter from './ConnectedCounter';
// import PureCounter from './PureCounter';
// import CounterToolbar from './CounterToolbar';

export default class MainView extends Component {
    render() {
        return (
            <div>
                <h1>Övning med Mobx</h1>
                <p>Installera mobx och skapa en counter med externt state. Lägg
                till knappar för att öka och minska räknarna.
                </p>
                <ConnectedCounter name="Alfa" />
                <div>
                    Räknarna måste inte ligga bredvid varandra.
                    <ConnectedCounter name="Beta" counter="b" color={NICE} />
                    <PureCounter name="Gamma" counter="a" />
                </div>

                <p>Knappar kan ligga var som helst nu.</p>
                <CounterToolbar counter="a" />
                <CounterToolbar counter="b" />
            </div>
        );
    }
}
