#Simple Mobx demo v2

This demo contains some simple react code with the mobx state management library. It uses the create-react-app to
configure some tools like WebPack.

It can be started by running the following commands:

```
> yarn install
> yarn start
```
(you need to install yarn first)
