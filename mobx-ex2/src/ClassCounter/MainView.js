import React, { Component } from 'react';
// import { NICE, SUPER_NICE, GOOD } from './colors';
import { NICE } from '../colors';
import ClassCounter from './ClassCounter';
import CounterToolbar from '../Common/CounterToolbar';
import MagicColorButton from '../Common/MagicColorButton';

export default class MainView extends Component {
    render() {
        return (
            <div>
                <h1>Mobx Demo - Class based</h1>
                <p>Add support for having a third counter.</p>
                <ClassCounter name="Alfa" counter="a" />
                <div>
                    The counter and the button don&apos;t have to be next to each other here.
                    <ClassCounter name="Beta" counter="b" color={NICE} />
                    <ClassCounter name="Epsilon" counter="a" />
                </div>

                <p>Here the buttons are separate.</p>
                <CounterToolbar counter="a" />
                <CounterToolbar counter="b" />

                <div>
                    <MagicColorButton color="red" />
                    <MagicColorButton color="darkgreen" />
                    <MagicColorButton color="" />
                </div>
            </div>
        );
    }
}
