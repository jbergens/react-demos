import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { observer } from 'mobx-react';
import { /* NICE, SUPER_NICE, */ GOOD } from '../colors';
// Using the store directly
import counterStore from '../Common/counterStore';

class Counter extends Component {
    render() {
        const key = this.props.counter || 'a';
        const actualColor = this.props.color || counterStore.color;

        console.log(`Counter[${key}].render()`, counterStore);

        const myValue = counterStore.counters[key] || 0; // Use the model/store
        // console.log(`Counter[${key}].render()`, this.props);

        return (
            <div>
                <h1 style={{ color: actualColor }}>
                    Counter ({this.props.name}:{key}): {myValue}
                </h1>
            </div>
        );
    }
}

Counter.propTypes = {
    name: PropTypes.string.isRequired,
    counter: PropTypes.string,
    color: PropTypes.string,
};

Counter.defaultProps = {
    counter: 'a',
    color: null, // GOOD,
};

// Wrap the CounterView and connect it to the observer implementation in mobx
const ClassCounter = observer(Counter);

export default ClassCounter;
