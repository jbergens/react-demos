import React, { Component } from 'react';
// import { NICE, SUPER_NICE, GOOD } from './colors';
import { NICE } from '../colors';
import PureCounter from './PureCounter';
import CounterToolbar from '../Common/CounterToolbar';
import MagicColorButton from '../Common/MagicColorButton';

export default class MainView extends Component {
    render() {
        return (
            <div>
                <h1>Mobx Demo - Functional components</h1>
                <p>Add support for having a third counter.</p>
                <PureCounter name="Alfa" counter="a" />
                <div>
                    The counter and the button don&apos;t have to be next to each other here.
                    <PureCounter name="Beta" counter="b" color={NICE} />
                    <PureCounter name="Gamma" counter="a" />
                </div>

                <p>Here the buttons are separate.</p>
                <CounterToolbar counter="a" />
                <CounterToolbar counter="b" />

                <div>
                    <MagicColorButton color="red" />
                    <MagicColorButton color="blue" />
                    <MagicColorButton color="" />
                </div>
            </div>
        );
    }
}
