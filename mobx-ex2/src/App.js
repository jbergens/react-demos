import React from 'react';
// eslint-disable-next-line
import Intro from './Intro';
// eslint-disable-next-line
import MainView from './Counter1/MainView';
// eslint-disable-next-line
import ClassMainView from './ClassCounter/MainView';

export default function App() {
    // return <Intro />;
    // return <ClassMainView />;
    return <MainView />;
}
