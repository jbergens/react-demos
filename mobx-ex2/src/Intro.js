import React from 'react';

export default function Intro() {
    return (
        <div>
            <h1>Mobx demos</h1>
            <p>Try to change this text and watch your
                web browser. Change to one of the extra
                components that are not used yet.
            </p>
        </div>
    );
}
