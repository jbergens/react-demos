import { action, observable, configure, makeAutoObservable } from 'mobx';
import { NICE } from '../colors';

// makeAutoObservable makeObservable
// Use strict in MobX (mobx4 syntax)
// configure({ enforceActions: 'observed' });

// Use ES2015 class and then use mobx decorators

class CounterStore {
    counters; // TODO: Make eslint happy
    color;

    constructor() {
        this.counters = { 'a': 0, 'b': 0 };
        this.color = null; // NICE;
        makeAutoObservable(this);
    }

    setColor(newColor) {
      console.log('setColor was called', newColor);
        this.color = newColor;
    }

    increase(key, increment) {
        console.log(`increaseAction was called. key: ${key}, increment: ${increment}`);

        const oldValue = this.counters[key];
        this.counters[key] = oldValue + (increment || 1);

        console.log('  new value: ', this.counters[key]);
    }

    decrease(key, increment) {
        console.log(`decrease was called. key: ${key}, increment: ${increment}`);

        const oldValue = this.counters[key];
        this.counters[key] = oldValue + (increment || -1);

        console.log('  new value: ', this.counters[key]);
    }
}

// Create a singleton instance  and export it
const counterStore = new CounterStore();

export default counterStore;
export { CounterStore }; // Also export the class for testing
