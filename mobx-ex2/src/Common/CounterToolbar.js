import React from 'react';
import PropTypes from 'prop-types';
import counterStore from './counterStore';
import CounterToolbarView from './CounterToolbarView';

const CounterToolbar = (props) => {
    const key = props.counter || 'a';

    return (
        <CounterToolbarView
            counter={key}
            increaseAction={() => counterStore.increase(key)}
            decreaseAction={() => counterStore.decrease(key)}
        />
    );
};
CounterToolbar.propTypes = {
    counter: PropTypes.string.isRequired,
};

export default CounterToolbar;
