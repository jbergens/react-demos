import React from 'react';
import PropTypes from 'prop-types';
// The "store" is more of a model
import counterStore from './counterStore';

const MagicColorButton = ({ color }) => (
    <button type="button" onClick={() => counterStore.setColor(color)}>
        Change color to {color}
    </button>
);

MagicColorButton.propTypes = {
    color: PropTypes.string.isRequired,
};

export default MagicColorButton;
