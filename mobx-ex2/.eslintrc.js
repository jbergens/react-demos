module.exports = {
    'extends': 'airbnb',
    globals: {
        'document': false,
        'it': false,
    },
    'parserOptions': {
        'ecmaVersion': 2018,
        'sourceType': 'module',
        'ecmaFeatures': {
            'jsx': true,
        },
    },
    env: {
        'es2017': true,
    },
    rules: {
        'indent': ['warn', 4],
        'lines-between-class-members': 0,
        'object-curly-spacing': ['error', 'always', {
            'arraysInObjects': false,
            'objectsInObjects': false,
        }],
        'no-unused-vars': 1,
        'no-console': 0,
        'object-curly-newline': 0,
        'quotes': ['error', 'single'],
        'quote-props': 0,
        'react/jsx-filename-extension': [1, { 'extensions': ['.js', '.jsx']}],
        'react/jsx-indent': [1, 4],
        'react/jsx-one-expression-per-line': [0, { 'allow': 'literal' }],
        'react/jsx-indent-props': [1, 4],
        'react/prefer-stateless-function': 0,
        'react/destructuring-assignment': 0,
        'linebreak-style': 0,
    },
};
